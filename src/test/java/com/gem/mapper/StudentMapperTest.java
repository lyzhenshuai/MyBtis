package com.gem.mapper;

import com.gem.entity.Student;
import com.gem.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentMapperTest {
    SqlSession sqlSession;
    StudentMapper studentMapper;


    @Before
    public void setup(){
        sqlSession = MyBatisUtil.getSession(true);
        studentMapper = sqlSession.getMapper(StudentMapper.class);

    }
    @Test
    public void selectStudentByID() {
        Student student = studentMapper.selectStudentByID(1l);
        System.out.println(student);
    }
    @After
    public void tearDown(){
        MyBatisUtil.close(sqlSession);
    }
}