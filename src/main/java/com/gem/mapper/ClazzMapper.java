package com.gem.mapper;

import com.gem.entity.Clazz;

public interface ClazzMapper {
    Clazz selectClazzByID(Long id);
}
