package com.gem.mapper;

import com.gem.entity.Student;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface StudentMapper {
    //级联查询，查询一个学生，级联查询出学生所在的班级
    //注释版
    //@Select("sel-ect s.*,c.id cid,c.name cname from mybatis_student s join mybatis_clazz c on s.clazz_id=c.id where s.id =#{id}")
    //ResultMap中值是xml中的 resultMap的id值
    //@ResultMap("scMap")
    Student selectStudentByID(Long id);
}
