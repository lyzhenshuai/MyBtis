package com.gem.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**反射创建类对象的工具*/
public class BeanFactory {

	//创建属性类对象
	static Properties prop;
	static {
		try {
			//静态代码块初始化属性类
			prop = new Properties();
			//获取本地配置文件的IO流对象
			InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("bean.properties");
			//属性类加载IO流对象
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取Bean对象
	 */
	public static Object getBean(String name) {
		Object obj = null;
		try {
			//根据属性类的KEY获取属性文件中的VALUE
			//获取类的Class实例
			Class<?> c = Class.forName(prop.getProperty(name));
			//反射创建对象
			obj = c.getConstructor().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
}
