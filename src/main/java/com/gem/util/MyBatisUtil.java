package com.gem.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 * mybatis工具类
 */
public class MyBatisUtil {

    static SqlSession sqlSession;
    static SqlSessionFactory sqlSessionFactory;

    static {
        try {
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("SqlMapConfig.xml"));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static SqlSession getSession(boolean autoCommit) {
        return sqlSessionFactory == null ? null : sqlSessionFactory.openSession(autoCommit);
    }

    public static void close(SqlSession sqlSession) {
        if (sqlSession != null) {
            sqlSession.close();
        }
    }
}
