drop table if exists mybatis_student;
drop table if exists mybatis_clazz;
create table if not exists mybatis_clazz(
id int primary key auto_increment,
name varchar(25) unique not null
);

create table if not exists mybatis_student(
id				int						primary key auto_increment,
name	varchar(25)  	unique not null,
gender	varchar(25),
birthday date,
clazz_id int,
constraint mybatis_student_clazzid_fk foreign key(clazz_id) references mybatis_clazz(id)
);

insert into mybatis_clazz values(1,'java');
insert into mybatis_clazz values(2,'pyython');

insert into mybatis_student values(1,'AA','1996-2-5',1);
insert into mybatis_student values(2,'BB','1996-3-5',2);
insert into mybatis_student values(3,'CC','1996-4-5',1);

commit;